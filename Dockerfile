FROM gitlab-registry.cern.ch/ci-tools/ci-worker:cc7

# LCG software stack
RUN . /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_97python3 x86_64-centos7-gcc9-opt; \
  export SCRAM_ARCH=slc7_amd64_gcc920

# Checkout and build MELA package
RUN git clone https://github.com/JHUGen/JHUGenMELA.git; \
  git chekcout 00cc82efec77a8dbc7c908f4f8203e5693e20e97; \
  cd JHUGenMELA; \
  ./setup.sh -j $(nproc) standalone; \
  cd ..

# Checkout and build MelaAnalytics
RUN git clone https://github.com/MELALabs/MelaAnalytics; \
  git checkout c81ac33828aa053228cc0ffa97a17ce6907402be; \
  cd MelaAnalytics; \
  for subpackage in CandidateLOCaster EventContainer GenericMEComputer; do \
    cd $subpackage; \
    make -j $(nproc); \
    cd ..; \
  done; \
  cd ..

# Set MELA environment
RUN export MELA_ROOT_DIR=$(pwd)/JHUGenMELA/MELA; \
  export MELA_ANALYTICS_ROOT_DIR=$(pwd)/MelaAnalytics; \
  export ROOT_INCLUDE_PATH=${ROOT_INCLUDE_PATH}:${MELA_ROOT_DIR}/interface
